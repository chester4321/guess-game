'use strict';

function changeMessage (newText){
    document.querySelector('.message').textContent = newText;
}

function changeNumber (newNumber){
    document.querySelector('.number').textContent = newNumber;
}

function changeScore (newScore){
    document.querySelector('.score').textContent = newScore;
}

let randNumber = Math.trunc(Math.random()*20)+1;
console.log(randNumber);
let score = 20;
let highscore = 0;

document.querySelector('.check').addEventListener('click', function(){
    const guess = Number(document.querySelector('.guess').value);
    console.log(guess);

// If there is no input
    if(!guess){
        changeMessage('✋ No number!');

// If the number is different
    }else if(guess !== randNumber){
        if(score == 1){
            changeMessage('😥 You lost');
            changeNumber(randNumber);
            changeScore(0);  
        }else{
            changeMessage(guess > randNumber ? '📈 Too high!' : '📉 Too low!');
            score--;
            changeScore(score);
        }

// If the number is correct
    }else if(guess === randNumber){
        changeMessage('🎉 Correct Number!');
        changeNumber(randNumber);
        document.querySelector('body').style.backgroundColor = '#60b347';
        document.querySelector('.number').style.width = '30rem';
        if(score > highscore){
            highscore = score;
           document.querySelector('.highscore').textContent = score;
        }
    }
})

document.querySelector('.again').addEventListener('click', function(){
    changeScore(20);
    changeNumber('?');
    changeMessage('Start guessing...');
    document.querySelector('.guess').value = '';
    document.querySelector('.number').style.width = '15rem';
    document.querySelector('body').style.backgroundColor = '#222';
    randNumber = Math.trunc(Math.random()*20)+1;
    score = 20;
    console.log(randNumber);
})